# Rick and Morty Character Search

## Overview
This project is a Java Spring Boot application that provides an API endpoint for searching Rick and Morty characters by name and retrieving information about their appearances in episodes, including the date of their first appearance. The application leverages the Rick and Morty API to fetch the required data.

## Prerequisites
- Java Development Kit (JDK) 8
- Apache Maven (for building the project)
- Docker (for running the application in a Docker container)
- Git (for version control)

## Getting Started
Follow these steps to set up and run the project:

### Cloning the Repository
1. Clone this GitLab repository to your local machine using the following command:
   git clone "repo url"

## Building the Project

cd rick-and-morty-character-search
mvn clean install

## Running Tests
mvn test

## Running the Application

java -jar target/rick-and-morty-character-search.jar

The API will be accessible at http://localhost:8080.

## Running with Docker
Build a Docker image using the provided Dockerfile:
docker build -t rick-and-morty-app .
Then
docker run -p 8080:8080 rick-and-morty-app

## API Endpoint
Endpoint for searching characters: GET /search-character-appearence at port 8080
Query parameter: name (character name)
example: http://localhost:8080/search-character-appearence?name=Rick+Sanchez
