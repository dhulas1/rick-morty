package com.dhulas.rickmorty.service;

import com.dhulas.rickmorty.exception.ApiRequestException;
import com.dhulas.rickmorty.model.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class CharacterServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ObjectMapper objectMapper;

    private CharacterService characterService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        characterService = new CharacterService(restTemplate, objectMapper);
    }

    @Test
    public void testGetCharacterInfo() throws ApiRequestException, IOException {
        //preparing mock variables
        String characterName = "Armagheadon";
        CharactersInfoResponse charactersInfoResponse = new CharactersInfoResponse();
        charactersInfoResponse.setInfo(createSampleInfo());
        charactersInfoResponse.setResults(createSampleCharacterResponseList());
        List<Episode> episodesResponse = new ArrayList<>();
        episodesResponse.add(createSampleEpisode("Ep1", 1, "November 17, 2019"));
        episodesResponse.add(createSampleEpisode("Ep2", 2, "November 18, 2019"));
        ResponseEntity<CharactersInfoResponse> responseEntity = new ResponseEntity<>(charactersInfoResponse, HttpStatus.OK);

        // Mocking restTemplate.getForEntity
        Mockito.when(restTemplate.getForEntity(Mockito.anyString(), Mockito.eq(CharactersInfoResponse.class)))
                .thenReturn(responseEntity);
        Mockito.when(restTemplate.getForEntity(Mockito.anyString(), Mockito.eq(Object.class)))
                .thenReturn(new ResponseEntity<>(episodesResponse, HttpStatus.OK));

        // Mocking objectMapper.convertValue
        Mockito.when(objectMapper.convertValue(Mockito.any(), Mockito.any(TypeReference.class))
                )
                .thenReturn(createSampleEpisodes());

        //running the method with mocks
        CharacterInfo characterInfo = characterService.getCharacterInfo(characterName);
        //asserting the vallues to ensure it match
        Assertions.assertEquals(characterName, characterInfo.getName());
        Assertions.assertEquals(2, characterInfo.getEpisodes().size());
        Assertions.assertEquals("November 17, 2019", characterInfo.getFirstAppearance());
    }

    // Helper method to create a sample Info
    private Info createSampleInfo() {
        Info info = new Info();
        info.setCount(1);
        info.setPages(1);
        info.setNext(null);
        info.setPrev(null);
        return info;
    }
    // Helper method to create a sample Episode
    private Episode createSampleEpisode(String name, int id, String date) {
        Episode episode = new Episode();
        episode.setId(id);
        episode.setName(name);
        episode.setAir_date(date);
        return episode;
    }

    // Helper method to create a sample character response
    private List<CharacterInfoResponse> createSampleCharacterResponseList() {
        List<CharacterInfoResponse> characterResponses = new ArrayList<>();
        CharacterInfoResponse characterResponse = new CharacterInfoResponse();
        characterResponse.setName("Armagheadon");
        characterResponse.setEpisode(createSampleCharacterEpisodes());
        // Set other attributes as needed
        characterResponses.add(characterResponse);
        return characterResponses;
    }

    // Helper method to create sample character episodes
    private List<String> createSampleCharacterEpisodes() {
        List<String> episodes = new ArrayList<>();
        episodes.add("https://rickandmortyapi.com/api/episode/1");
        episodes.add("https://rickandmortyapi.com/api/episode/2");
        return episodes;
    }
    private List<Episode> createSampleEpisodes() {
        List<Episode> episodesResponse = new ArrayList<>();
        episodesResponse.add(createSampleEpisode("Ep1", 1, "November 17, 2019"));
        episodesResponse.add(createSampleEpisode("Ep1", 1, "November 17, 2019"));
        return episodesResponse;
    }
}
