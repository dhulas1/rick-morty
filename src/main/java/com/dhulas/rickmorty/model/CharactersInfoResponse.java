package com.dhulas.rickmorty.model;

import java.util.List;

public class CharactersInfoResponse {
    private Info info;
    private List<CharacterInfoResponse> results;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<CharacterInfoResponse> getResults() {
        return results;
    }

    public void setResults(List<CharacterInfoResponse> results) {
        this.results = results;
    }
}
