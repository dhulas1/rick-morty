package com.dhulas.rickmorty.model;

import java.time.LocalDate;
import java.util.List;

public class CharacterInfoBuilder {
    private String name;
    private List<String> episodes;
    private String firstAppearance;

    public CharacterInfoBuilder name(String name) {
        this.name = name;
        return this;
    }

    public CharacterInfoBuilder episodes(List<String> episodes) {
        this.episodes = episodes;
        return this;
    }

    public CharacterInfoBuilder firstAppearance(String firstAppearance) {
        this.firstAppearance = firstAppearance;
        return this;
    }

    public CharacterInfo build() {
        CharacterInfo characterInfo = new CharacterInfo();
        characterInfo.setName(name);
        characterInfo.setEpisodes(episodes);
        characterInfo.setFirstAppearance(firstAppearance);
        return characterInfo;
    }
}
