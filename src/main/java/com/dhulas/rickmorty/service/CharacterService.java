package com.dhulas.rickmorty.service;

import com.dhulas.rickmorty.exception.ApiRequestException;
import com.dhulas.rickmorty.model.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.dhulas.rickmorty.utils.ServiceUtils;

import java.util.*;

@Service
public class CharacterService {
    // Define constant URLs for the Rick and Morty API
    private static final String CHAR_API_URL = "https://rickandmortyapi.com/api/character";
    private static final String EPISODE_API_URL = "https://rickandmortyapi.com/api/episode/";

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public CharacterService(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    public CharacterInfo getCharacterInfo(String name) throws ApiRequestException {
        try {
            ResponseEntity<CharactersInfoResponse> templateResponse = restTemplate.getForEntity(
                    ServiceUtils.buildCharacterInfoUrl(name, CHAR_API_URL),
                    CharactersInfoResponse.class);

            if (templateResponse.getStatusCode() == HttpStatus.OK) {
                CharactersInfoResponse responseBody = templateResponse.getBody();
                List<Episode> allCharEpisodes = getAllCharEpisodes(responseBody);
                return new CharacterInfoBuilder()
                        .name(name)
                        .episodes(ServiceUtils.getEpiosodeNames(allCharEpisodes))
                        .firstAppearance(allCharEpisodes.get(0).getAir_date())
                        .build();
            } else {
                // Handle the case where the response status is not OK
                throw new ApiRequestException("Error while fetching info from Rick and Morty Api");
            }
        } catch (Exception e) {
            // Handle exceptions and throw a custom ApiRequestException with a specific message
            throw new ApiRequestException("Error while fetching character info from Rick and Morty Api");
        }
    }

    public List<Episode> getAllCharEpisodes(CharactersInfoResponse response) throws ApiRequestException{
        List<CharacterInfoResponse> allEpisodes = new ArrayList<>();
        allEpisodes.addAll(response.getResults());

        while (response.getInfo().getNext()!=null){
            response = restTemplate.getForObject(response.getInfo().getNext(),CharactersInfoResponse.class);
            allEpisodes.addAll(response.getResults());
        }

        return getCharacterEpisodes(ServiceUtils.getMultipleEpisodeQuery(
                ServiceUtils.getPageCharEpisodes(allEpisodes)));
    }

    public List<Episode> getCharacterEpisodes(String allCharEpisodesId) throws ApiRequestException{

        ResponseEntity<Object> responseEntity = restTemplate.getForEntity(EPISODE_API_URL + allCharEpisodesId, Object.class);

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            Object responseBody = responseEntity.getBody();
            if (responseBody instanceof List) {
                return objectMapper.convertValue(responseBody, new TypeReference<List<Episode>>() {
                });

            } else if (responseBody instanceof HashMap) {
                return Collections.singletonList(objectMapper.convertValue(responseBody, Episode.class));
            } else {
                return Collections.emptyList();
            }
        }else{throw new ApiRequestException("Error while fetching info from Rick and Morty Api");}
    }
}
