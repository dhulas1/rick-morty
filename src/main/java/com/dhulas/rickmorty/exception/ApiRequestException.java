package com.dhulas.rickmorty.exception;

public class ApiRequestException extends Exception {
    public ApiRequestException(String message) {
        super(message);
    }
}
