package com.dhulas.rickmorty.utils;

import com.dhulas.rickmorty.model.CharacterInfoResponse;
import com.dhulas.rickmorty.model.Episode;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

public class ServiceUtils {
    public static String getMultipleEpisodeQuery(List<String> strings) {
        return strings.stream()
                .map(s -> s.substring(s.lastIndexOf('/') + 1))
                .collect(Collectors.joining(","));
    }

    public static List<String> getEpiosodeNames(List<Episode> episodes) {
        return episodes.stream()
                .map(Episode::getName)
                .collect(Collectors.toList());
    }

    public static List<String> getPageCharEpisodes(List<CharacterInfoResponse> allCharInfoResponse){
        return allCharInfoResponse.stream()
                .flatMap(charInfoResponse -> charInfoResponse.getEpisode().stream())
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public static String buildCharacterInfoUrl(String name, String BaseUri) {
        return UriComponentsBuilder.fromHttpUrl(BaseUri)
                .queryParam("name", name.replace(" ", "+"))
                .toUriString();
    }

}
