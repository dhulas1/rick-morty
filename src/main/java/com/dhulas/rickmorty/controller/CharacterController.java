package com.dhulas.rickmorty.controller;

import com.dhulas.rickmorty.exception.ApiRequestException;
import com.dhulas.rickmorty.model.CharacterInfo;
import com.dhulas.rickmorty.service.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping()
public class CharacterController {

    private final CharacterService characterService;

    @Autowired
    public CharacterController( CharacterService characterService){
        this.characterService = characterService;
    }

    @GetMapping("/search-character-appearence")
    public ResponseEntity<CharacterInfo> getCharacterInfo(@RequestParam String name) throws ApiRequestException {
        try {
            // Get character information from the service
            CharacterInfo characterInfo = characterService.getCharacterInfo(name);
            if (characterInfo != null) {
                return new ResponseEntity<>(characterInfo, HttpStatus.OK);
            } else {
                // Return a response indicating the character was not found
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            // Handle exceptions and throw a custom ApiRequestException
            throw new ApiRequestException("Error while fetching info from Rick and Morty Api");
        }

    }
}
