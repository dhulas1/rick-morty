# Use a base image with Java 11 (OpenJDK)
FROM openjdk:11

# Set the working directory in the container
WORKDIR /app

# Copy the application JAR file (make sure the JAR file is in the same directory as the Dockerfile)
COPY target/rick-morty-0.0.1-SNAPSHOT.jar .

# Expose the port on which the Spring Boot application will run
EXPOSE 8080

# Command to run the Spring Boot application
CMD ["java", "-jar", "rick-morty-0.0.1-SNAPSHOT.jar"]
